<?php
namespace Sani;
use PDO;
use PDOException;

class Cate{
    private $conn = '';
    public $name = '';
    public $description = '';
    public function __construct()
    {
        try{
            session_start();
            $this->conn = new PDO ("mysql:host=localhost;dbname=class_21","root","admin");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }

    public function index()
    {
        $query = "SELECT * FROM category";
        $stmt = $this->conn->query($query);
        return $stmt->fetchAll();

    }

    public function setData(array $data = [])
    {
        $error = [];
        if(array_key_exists('name', $data) && !empty($data['name'])){
            $this->name = $data['name'];
        }else{
            $error[] = 'Name Requird';
        }

        if(array_key_exists('description', $data) && !empty($data['description'])){
            $this->description = $data['description'];
        }else{
            $error[] = 'description Requird';
        }
        
        if(count($error)){
            $_SESSION['error'] = $error;
            header('location: create.php');
        }else{
            return $this;
        }
        
    }

    public function store()
    {
            try{
                $query ="INSERT INTO category(name, description) VALUES(:name , :description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':name' => $this->name,
                ':description' => $this->description
            ));
            $_SESSION['message'] ='Successfully Created !';
            header('Location:index.php');
            }catch(PDOException $e){
                echo $e->getMessage();
            }


    }

    public function show($id)
    {
        $query = "SELECT * FROM category where id=".$id;
        $stmt = $this->conn->query($query);
        return $stmt->fetch();

    }

    public function update($id)
    {
        $query ="UPDATE category SET name=:name, description=:description where id= ".$id;
        $stmt = $this->conn->prepare($query);
        $stmt ->execute(array(
        ':name' => $this->name,
        ':description' => $this->description
        ));
        $_SESSION['message'] ='Successfully Update !';
        header('location: index.php');

    }

    public function delete($id)
    {
        $query ="delete from category where id=".$id;
        $stmt = $this->conn->query($query);
        $stmt ->execute();
        $_SESSION['message'] ='Successfully Delete !';
        header('location:index.php');

    }
}






?>