<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD</title>
    <!--bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>
        h2{
            margin-bottom: 10px;
            text-align: center;
        }
    </style>
</head>
<body>

<?php

require_once '../../src/category.php';
use Sani\Cate;
$cate = new Cate;
$show = $cate->show($_GET['id']);

?>

<h2>Update Data</h2>

<form action="update.php" method="POST" class="container" style="max-width:500px;">

  <div class="form-group">
  <input type="hidden" name="id" value="<?= $show['id'] ?>">
  </div>
  <div class="form-group">
    <label for="Name">Name</label>
    <input type="text" value="<?= $show['name'] ?>" class="form-control" name="name" placeholder="Input Category Name">
  </div>

  <div class="form-group">
    <label for="Name">Description</label>
    <input type="text" value="<?= $show['description'] ?>" class="form-control" name="description" placeholder="Input Description">
  </div>

  <div style="width: 100px; margin:0 auto;">
    <button type="submit" class="btn btn-primary">Update</button>
  </div>
</form>



<!--bootstrap js-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>