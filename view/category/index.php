<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD</title>
    <!--bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>
        h2{
            margin-bottom: 10px;
            text-align: center;
        }
        table{
            text-align: center;
        }
        
    </style>
</head>
<body>

<?php

require_once '../../src/category.php';
use Sani\Cate;
$cate = new Cate;
$show = $cate->index();


?>

<?php 

if(isset($_SESSION['message'])){?>
    <table  style="width: 450px; margin:0 auto; margin-bottom:5px; text-align:center;">
      <tbody>
        <td><?php echo $_SESSION['message']; ?></td>
      </tbody>
    </table>
  
  

<?php }  unset($_SESSION['message']);?>



<h2>Category Info</h2>
<table class="table table-striped container">
  <thead>
    <tr>
      <th scope="col" style="width: 100px;">ID</th>
      <th scope="col" style="width: 300px;">Name</th>
      <th scope="col">Description</th>
      <th scope="col" style="width: 80px;"><a class="btn btn-primary" href="create.php" role="button">Add</a></th>
      <th scope="col" style="width: 80px;"><a class="btn btn-primary" href="#" role="button">Show</a></th>
      <th scope="col" style="width: 80px;"><a class="btn btn-primary" href="create" role="button">Edit</a></th>
      <th scope="col" style="width: 80px;"><a class="btn btn-primary" href="create" role="button">Delete</a></th>
      
      
    </tr>
  </thead>
  <tbody>
    <?php foreach($show as $show){ ?>
      <tr>
        <td><?= $show['id'] ?></td>
        <td><?= $show['name'] ?></td>
        <td><?= $show['description'] ?></td>
        <td scope="col" style="width: 80px;"><a class="btn btn-primary" href="create.php" role="button">Add</a></td>
        <td scope="col" style="width: 80px;"><a class="btn btn-primary" href="show.php?id=<?= $show['id'] ?>" role="button">Show</a></td>
        <td scope="col" style="width: 80px;"><a class="btn btn-primary" href="edit.php?id=<?= $show['id'] ?>" role="button">Edit</a></td>
        <td scope="col" style="width: 80px;"><a class="btn btn-primary" href="delete.php?id=<?= $show['id'] ?>" role="button">Delete</a></td>
      </tr>
    <?php } ?>
  </tbody>
</table>


<!--bootstrap js-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>